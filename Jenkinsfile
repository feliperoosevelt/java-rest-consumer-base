String sectionHeaderStyle = '''
    color: white;
    background: gray;
    font-family: Roboto, sans-serif !important;
    padding: 3px;
    text-align: center;
'''

String separatorStyle = '''
    border: 0;
    border-bottom: 1px #ccc;
    background: #999;
'''

properties([
    disableConcurrentBuilds(),
    [$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '5', numToKeepStr: '10']],
    parameters([                  
        [
            $class: 'ParameterSeparatorDefinition',
            name: 'BUILDER_HEADER',
            sectionHeader: 'Docker Builder Parameters',
            separatorStyle: separatorStyle,
            sectionHeaderStyle: sectionHeaderStyle
        ],
        booleanParam(defaultValue: false, description: 'Update builder', name: 'makeBuilder'),
        string(
            name: 'builderVersion',
            defaultValue: 'x.x.x',
            description: 'Type the version of the docker container to be created.'
        ),        
        [
            $class: 'ParameterSeparatorDefinition',
            name: 'BUILD_HEADER',
            sectionHeader: 'Build Parameters',
            separatorStyle: separatorStyle,
            sectionHeaderStyle: sectionHeaderStyle
        ],
        booleanParam(defaultValue: true, description: 'Build', name: 'build'),
        string(
            name: 'selectedBuilderVersion',
            defaultValue: '1.0.0',
            description: 'Type the version of the docker container to be used in the build stage.'
        )
    ])
])

@Library("neppochat") _
node{
    
    def pipeline    = new com.neppo.NeppoLib()
    
    def projectName = "java-lib-rest-consumer"
    def buildername = "java-lib-rest-consumer-builder"
    def dockerRegistryCredentials = "dockerRegistryCredentials"
    def bitbucketCredentials = "jenkins"
    def builderImageVersion  = "${params.selectedBuilderVersion}"
    def m2path = "\$HOME/.m2"

    pipeline.cleanDir()
    pipeline.cloneRepository(projectName)
    pipeline.updateBuilderImage(buildername, //Builder image name
                                dockerRegistryCredentials //Docker Registry Credentials id
                                )
    
    pipeline.pullBuilder(buildername,  // Builder image name
                            builderImageVersion, // Builder image version
                            dockerRegistryCredentials // Docker Registry Credentials id
                            )

    def shortCommit = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
    
    pipeline.buildJavaLibs(
        buildername, // Builder image name
        builderImageVersion, // Builder image version
        "mvn -s /m/settings.xml clean deploy -DbuildNumber=hash${shortCommit} -DskipTests", // Effective build command
        null, // Test build command
        m2path, // jenkins .m2 directory path
        projectName, // project name used in rocket messaging
    )

}